package Ext::store;
use Moose;
with 'Ext::store::Role::HasConfig';

has 'query_type' => ( isa => 'Str', is=>'rw', default=>'Catalyst');

1;
