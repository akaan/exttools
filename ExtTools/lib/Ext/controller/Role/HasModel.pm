package Ext::controller::Role::HasModel;
use Moose::Role;

has 'model_base' => ( is => 'rw', isa => 'Str' );

has 'model_name' => ( is => 'rw', isa => 'Str' );
has 'model_class' =>
  ( is => 'rw', isa => 'Str', default => 'Ext::store::Model' );

sub get_model {
    my ( $self, $c, $file ) = @_;
    my %model_config = ();
    $c->debug("loading model configuration ");
    $model_config{'model_name'} = $self->model_name if ( $self->model_name );
    $model_config{'model_base'} = $self->model_base if ( $self->model_base );
    my $model = $self->model_class->new(%model_config);
    $model->query_type('Catalyst');
    return $model;
}
1;