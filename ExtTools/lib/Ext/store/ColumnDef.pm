package Ext::store::ColumnDef;

use Moose;
use Data::Dumper;

our @COLS = qw(
  dataIndex
  id
  text
  flex
  align
  width
  sortable
  hideable
  renderer
  editor
);

# column definitions
for my $name (@COLS) {
	has $name => (
		is      => 'rw',
		isa     => 'Str',
		lazy    => 1,
		default => ''
	);
}

sub BUILD {
	my ( $self, $args ) = @_;
	my $element = $args->{'element'};
	#print Dumper($element);
	return unless $element;
	foreach my $field (@COLS) {
		if ( $element and $element->can($field) ) {
			#printf "%s [%s]\n", $field, '';    #, $element->can($field);
			$self->$field( $element->$field )
			  if $self->can($field);
		}
	}
	# setting defaults
	foreach my $x (qw/dataIndex id text/) {
		$self->$x($element->name) unless $self->$x;
	}
}

sub render {
	my ($self)   = @_;
	my $string   = "{ \n%s\n },\n";
	my $template = "";
	foreach my $k (@COLS) {
		$template .= sprintf "  '%s' : '%s', \n", $k, $self->$k ? $self->$k : ''
		  if $self->$k;
	}
	return sprintf $string, $template;

}

1;
