package Ext::store::Store;
use Moose;
use Carp qw/croak/;
with 'Ext::debug', 'Ext::store::Role::HasStore';

has 'resultset' => ( is => "rw", isa => 'Str', default => 'data' );
has 'schema' => ( is => "rw", isa => 'Str' );
has 'prefetch' => ( is => 'rw', isa => 'HashRef' );
has 'order' => ( is => 'rw', isa => 'ArrayRef', default => sub { [] } );
has 'find_method' => ( is => 'rw', isa=>'Str', default => 'find');

sub BUILD {
	my ( $self, $config ) = @_;
	$self->construct($config);
}

sub schema_ref {
    my ($self) = @_;
    croak "Need resultset and schema"
      unless ( $self->resultset && $self->schema );
    return join( '::', $self->schema, $self->resultset );
}

1;

