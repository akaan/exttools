# innerclass for the element representation
package Ext::store::Element;
use Moose;
use Scalar::Util qw(blessed reftype);
use Ext::store::ColumnDef;
use Carp qw/croak/;
use Data::Dumper;
with 'Ext::store::Role::ToExt';

has 'type' => ( is => 'rw', isa => 'Str', default => 'string' );
has 'name' => ( is => 'rw', isa => 'Str' );
has 'label'       => ( is => 'rw', isa => 'Str' );
has 'elements'    => ( is => 'rw', isa => 'ArrayRef', default => sub { [] } );
has 'constraints' => ( is => 'rw', isa => 'ArrayRef', default => sub { [] } );
has 'columnDef'   => ( is => 'rw', isa => 'Object' );
has 'store' => (is => 'rw', isa=>'Str'); # for storable object

my $tick = undef;

sub BUILD {
	my ( $self, $config ) = @_;
	#print Dumper($config);
	$self->resolve_elements( $config->{'elements'} ) if $config->{'elements'};
	my $coldef = Ext::store::ColumnDef->new( 'element' => $self );
	$self->columnDef($coldef);
	#print Dumper($self);

}

sub resolve_elements {
	my ( $self, $_elements ) = @_;
	$self->type('array');
	foreach my $_elem ( @{$_elements} ) {

		# do not push elements that are already processed.
		#print reftype($_elem) . $self->Dumper($_elem);
		next if ( ref($_elem) ne 'HASH' );
		my $newelem = Ext::store::Element->new($_elem);
		my $label   = $_elem->{'label'};
		$label = $newelem->name unless ($label);
		$label =~ s/\./_/g;
		$newelem->label($label) unless $newelem->label;

		push @{ $self->elements }, $newelem;
	}

	# remove all non objects
	my @arr = @{ $self->elements };
	@arr = grep { ref($_) eq 'Ext::store::Element' } @arr;
	$self->elements( \@arr );
}

sub form_data {
	my ($self) = @_;
	my %form = ();
	foreach (qw/name label type/) {
		$form{$_} = $self->$_;
	}
	return \%form;
}

sub to_ext_mapping {
	my ( $self, $args ) = @_;
	if ($self->name =~ /\./) {
	  # only the last part of the dot field 
	  # is the value for the field response ie 
	  # parent.child -> the field should be 'child' so
	  # child, parent -> m[0] = child; 
	  
		my @methods = reverse split /\./, $self->name; #/
		$self->name($self->label ? $self->label : $methods[0]);
		
		
	}
	my %field = (
		'name'    => $self->name,
		'mapping' => $self->name,
		'type'    => 'auto',

		#		'value'   => $self->label ? $self->label : $self->name
	);
	return \%field;
}

1;

#           "fields"
#           : [
#               { "mapping" : "code",  "name" : "code",  "type" : "string" },
#               { "mapping" : "name",  "name" : "name",  "type" : "string" },
#               { "mapping" : "alias", "name" : "alias", "type" : "string" },
#               {
#                   "mapping" : "style_type",
#                   "name"    : "style_type",
#                   "type"    : "string"
#               },
#               {
#                   "mapping" : "supplier_code",
#                   "name"    : "supplier_code",
#                   "type"    : "string"
#               },
#               {
#                   "mapping" : "sale_price",
#                   "name"    : "sale_price",
#                   "type"    : "string"
#               },
#               {
#                   "mapping" : "purchase_cost",
#                   "name"    : "purchase_cost",
#                   "type"    : "string"
#               },
#               { "mapping" : "rooted", "name" : "rooted", "type" : "string" },
#               {
#                   "mapping" : "description",
#                   "name"    : "description",
#                   "type"    : "string"
#               }
#           ],
