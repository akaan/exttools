# innerclass for the element representation
package Ext::store::Element;
use Moose;
use Scalar::Util qw(blessed reftype);
use Carp qw/croak/;
with 'Ext::debug', 'Ext::store::Role::ToExt';

has 'type' => ( is => 'rw', isa => 'Str', default => 'string' );
has 'name' => ( is => 'rw', isa => 'Str' );
has 'label' => ( is => 'rw', isa => 'Str' );
has 'elements' => ( is => 'rw', isa => 'ArrayRef', default => sub { [] } );

my $tick = undef;

sub BUILD {
	my ( $self, $config ) = @_;
	$self->resolve_elements( $config->{'elements'} ) if $config->{'elements'};
}

sub resolve_elements {
	my ( $self, $_elements ) = @_;
	$self->type('array');
	foreach my $_elem ( @{$_elements} ) {

		# do not push elements that are already processed.
		print reftype($_elem) . $self->Dumper($_elem);
		next if ( ref($_elem) ne 'HASH' );
		my $newelem = Ext::store::Element->new($_elem);
		my $label   = $_elem->{'label'};
		$label = $newelem->name unless ($label);
		$label =~ s/\./_/g;
		$newelem->label($label) unless $newelem->label;

		#die $self->Dumper($newelem);
		push @{ $self->elements }, $newelem;
	}

	# remove all non objects
	my @arr = @{ $self->elements };
	@arr = grep { ref($_) eq 'Ext::store::Element' } @arr;
	$self->elements( \@arr );
}

sub to_ext_mapping {
	my ( $self, $args ) = @_;
	my %field = (
		'name'    => $self->name,
		'mapping' => $self->name,
		'type'    => 'auto',

		#		'value'   => $self->label ? $self->label : $self->name
	);
	return \%field;
}

1;

#           "fields"
#           : [
#               { "mapping" : "code",  "name" : "code",  "type" : "string" },
#               { "mapping" : "name",  "name" : "name",  "type" : "string" },
#               { "mapping" : "alias", "name" : "alias", "type" : "string" },
#               {
#                   "mapping" : "style_type",
#                   "name"    : "style_type",
#                   "type"    : "string"
#               },
#               {
#                   "mapping" : "supplier_code",
#                   "name"    : "supplier_code",
#                   "type"    : "string"
#               },
#               {
#                   "mapping" : "sale_price",
#                   "name"    : "sale_price",
#                   "type"    : "string"
#               },
#               {
#                   "mapping" : "purchase_cost",
#                   "name"    : "purchase_cost",
#                   "type"    : "string"
#               },
#               { "mapping" : "rooted", "name" : "rooted", "type" : "string" },
#               {
#                   "mapping" : "description",
#                   "name"    : "description",
#                   "type"    : "string"
#               }
#           ],
