package Ext::store::HashModel;
use Moose::Role;

use Ext::store::Store;
use Ext::store::Element;
use Hash::Merge::Simple qw(merge);
use Carp qw/croak confess cluck/;


# Generating the grid data as records;
#           {
#               "name"          : "Courtney",
#               "rooted"        : 0,
#               "sale_price"    : 149,
#               "description"   : null,
#               "supplier_code" : "GWS",
#               "alias"         : "Courtney",
#               "style_type"    : 1,
#               "code"          : "FAD3",
#               "purchase_cost" : 74.5
#           },
sub object_data {
  my ($self,$data,$param) = @_;
  my $targetrow = $self->to_ext_ref($data);
  return merge $targetrow, $param;  
}

# Generating the row data as records;
#           {
#               "name"          : "Courtney",
#               "rooted"        : 0,
#               "sale_price"    : 149,
#               "description"   : null,
#               "supplier_code" : "GWS",
#               "alias"         : "Courtney",
#               "style_type"    : 1,
#               "code"          : "FAD3",
#               "purchase_cost" : 74.5
#           },

sub grid_data {
	my ( $self, $data, $param ) = @_;
	my @rows = ();

	# populate the structure with values
	$self->to_ext_array( \@rows, $data, 1 );
  # cluck 'here';
	#   my $cc   = 0;
	#   foreach my $record (@$data) {
	#       my %row = ();
	#       foreach my $e ( @{ $self->elements } ) {
	#           next unless $e;
	#           $e->to_ext(\%row,$record);
	#       }
	#       push @rows, \%row;
	#   }
	my $return = {
		results     => scalar @rows,
		$self->root => \@rows,
		metaData    => {
			totalProperty   => 'results',
			root            => $self->root,
			idProperty      => 'crc',
			fields          => $self->to_ext_mapping,
			messageProperty => 'message',
		}
	};
	return merge $return, $param;
}

1;
