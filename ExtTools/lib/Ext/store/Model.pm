package Ext::store::Model;
use Moose;
extends 'HTML::FormFu';
with 'Ext::debug', 'Ext::store::Role::ToExt', 'Ext::store::Role::HasConfig','Ext::store::HashModel';

use Ext::store::Store;
use Ext::store::Element;
use HTML::FormFu::ExtJS 0.076;
use Ext::form::FormPanel;
use Hash::Merge::Simple qw(merge);
use Carp qw/croak/;

has 'query_type' => ( isa => 'Str', is => 'rw', default => 'Catalyst' );
has 'model_name' => ( is => 'rw', isa => 'Str' );
has 'model_base' => ( is => 'rw', isa => 'Str', default => 'root/forms' );
has 'store'      => ( is => 'rw', isa => 'Ext::store::Store' );
has 'elements'   => ( is => 'rw', isa => 'ArrayRef', default => sub { [] } );
has 'root' => ( is => 'rw', isa => 'Str', default => 'data' );
has 'model' => ( is => 'rw', isa => 'Str' );

sub BUILD {
	my ( $self, $args ) = @_;

	if ($args) {
		printf "loading %s from supplied constructor arguments\n", ref($self);
		my @methods = qw/model_name name model_base/;
		foreach my $m (@methods) {
			if ( $self->can($m) and $args->{$m} ) {
				$self->$m( $args->{$m} );
			}
		}
	}

	if ( $self->name and !$self->model_name ) {
		$self->model_name( $self->name );
	}
	unless ( $self->model_name ) {
		croak sprintf "No model name set for %s", $self->name;
	}
	#
	# Loading the yml file with the model defenitions 
	#
	my $file = sprintf "%s/%s.yml", $self->model_base, $self->model_name;
	printf "reading %s ...\n", $file;

	my $config = $self->load_config_file($file);
	if ( $config->{'store'} ) {

		$self->store( Ext::store::Store->new( $config->{'store'} ) );
		delete $config->{'store'};
	}
	else {
		#croak sprintf "No store found for $self->name with %s", Dumper($config);
	}
	if ( $config->{'model'} ) {

		$self->model( $config->{'model'} );
		delete $config->{'model'};
	}
	else {
		croak sprintf "No model found for $self->name with %s, make sure in the 'forms/*.yml' form definintion.", Dumper($config);
	}
	if ( $config->{'elements'} ) {

		#croak sprintf "No elements found for $self->name with %s",
		#  Dumper($config);
		my @elems = @{ $config->{'elements'} };
		if (@elems) {

			foreach my $elem (@elems) {
				push @{ $self->elements }, Ext::store::Element->new($elem);
			}
		}
	}
	$self->form_data($config);
}

sub to_form {
	my ( $self, $c, $file ) = @_;
	
	my %config = ( 'model' => $self);
	my $panel = Ext::form::FormPanel->new(%config);
	return $panel;
}

sub model_ref {
	my ($self) = @_;
	my $store = $self->store;
	croak "Need resultset and schema"
	  unless ( $store->resultset && $store->schema );
	croak sprintf "Model was not defined unable to return reference '%s'", $self->model unless $self->model;
	my $_temp = join( '::', $self->model, $store->resultset );
	return $_temp;
}

__PACKAGE__->meta->make_immutable;

sub dump {
	my ($self) = @_;
	print Dumper($self);
}

sub form_data {
	my ($self, $data, $param) = @_;
	#print 'herte os spomethin wronk', Dumper({ data=>$data, param=>$param});
    my $return = {
        action=>$self->model_name,
        method=>'update',
        type=>'rpc',
        'tid'=>3,
        result => { 'success'=>1}
    };
    print "form_returns: " . Dumper($return);
    return merge $return, $param;
}



sub to_ext_mapping {
	my ($self) = @_;
	my @mapping = ();
	foreach my $e ( @{ $self->elements } ) {
		push @mapping, $e->to_ext_mapping;
	}
	return \@mapping;
}

1;
