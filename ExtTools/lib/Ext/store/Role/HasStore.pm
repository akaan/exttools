package Ext::store::Role::HasStore;
use Moose::Role;
use Carp qw/croak/;

has 'store' => ( is => 'rw', isa => 'Ext::store::Store', );

sub construct {
	my ( $self, $config ) = @_;
	unless ($config) {
		croak "Missing configuration: " . $config;
	} 
	my @methods = qw/resultset schema model prefetch order/;
	foreach my $m (@methods) {
		if ($self->can($m) and $config->{$m}) {
			$self->$m($config->{$m});
		}
	}
}

1;
