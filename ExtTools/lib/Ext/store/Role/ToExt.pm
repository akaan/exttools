package Ext::store::Role::ToExt;
use Moose::Role;
use Data::Structure::Util qw( unbless );
use Carp qw/croak cluck/;
use Data::Dumper;

sub to_ext_array {
  my ( $self, $rowref, $data, $root ) = @_;

#croak "missing name:" . Dumper($self), Dumper($rowref) unless $self->name and not $root;
  confess "RowRef not an array ", Dumper($rowref) if ref($rowref) ne 'ARRAY';
  foreach my $record (@$data) {
    my %row = ();
    foreach my $e ( @{ $self->elements } ) {
      next unless $e and $record;
      $e->to_ext( \%row, $record );
    }
    push @{$rowref}, \%row;
  }
  printf "\nprocessed : %d records.\n", scalar @{$rowref};
  #print Dumper($rowref);
}

sub to_ext_ref {
  my ( $self,  $record ) = @_;

#croak "missing name:" . Dumper($self), Dumper($rowref) unless $self->name and not $root;
  confess "RowRef must not be an array ", Dumper($record) if ref($record) eq 'ARRAY';
    my %row = ();
    foreach my $e ( @{ $self->elements } ) {
      next unless $e and $record;
      $e->to_ext( \%row, $record );
    }
    return \%row;
}


sub to_ext {
  my ( $self, $row, $record ) = @_;
  my $key = $self->name;
  if ( $key =~ /\./ and $record ) {
    $self->to_ext_nested( $row, $record );

  }
  elsif ( $record and $record->can($key) ) {
    my $value = $record->$key;
    if ( blessed $value ) {
      if ( $self->type eq 'array' ) {
        my @array = ();
        my @data  = $value->all();
        $self->to_ext_array( \@array, \@data );
        $row->{$key} = \@array if @array;
      }
      else {

        #die Dumper($value->get_columns), "-",ref $value;
        if ( $self->type eq 'object' ) {
          my %object = $value->get_columns;
          $row->{$key} = \%object;    #, $self->name;
        }
        else {
          $row->{$key} = $value;
        }

   #croak sprintf "date[%s]: %s\n", $key, $row->{$key}  if $self->name =~ /dt$/;
      }
    }
    else {
      $row->{$key} = $value;
    }
  }
  else {
    if ($record) {
      #print Dumper(  $record);
    }
    else {
      croak "record is empty for $row";
    }
  }
}

sub to_ext_nested {
  my ( $self, $rowref, $record ) = @_;
  my $childvalue = $self->name;
  my @methods    = split /\./, $childvalue;    #/

  # if there is only one element stuff the method array
  @methods = ($childvalue) unless @methods;

  #print Dumper( \@methods, ref $record  );
  my $ref = $record;
  $childvalue =~ s/\./_/g;

  # traverse the child record fields.
  my $path = ref $ref . '.';
  my $m = undef;
  while (@methods) {
    $m = shift @methods;
    $path .=  "$m.";
    if ($ref) {
      if ( $ref->can($m) ) {

        # assign the first part of the object.method = object as the label
        # unless specified in the yml.

#        printf "assign: %s=%s\n", $path, $ref;
#        if ($ref) {
#          #print '---';
#          #printf "ref[%s] = %s\n", ref $ref, $ref->$m ; 
##           if ref $ref =~ /OsVersion/;          
#        } else {
#          printf "assign.ref->m %s\n", $ref->$m;
#        }
        $ref = $ref->$m;
        last unless $ref; # break out if the reference is undefined.

      }
      else {
        croak "cannot reference method '$m' on '$ref' for $childvalue";

      }
    }
    else {
      croak "cannot reference method '$m' on undefined ref for $childvalue";
    }
  }
  #print Dumper($self);
  #printf "----->> %s rowref[%s] = %s\n", $path, $self->label ? $self->label : $m, $ref ? $ref : '???';
  $rowref->{ $self->label ? $self->label : $m } = $ref ? $ref : '';

  #printf ">>>>>>> RowRef: %s", Dumper($rowref);
}

1;
