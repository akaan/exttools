package Ext::store::Role::HasElements;
use Moose::Role;

has 'elements' => ( is=>'rw', isa=>'ArrayRef', default=> sub {[]});

1;
