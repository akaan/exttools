package Ext::store::Role::HasConfig;
use Config::Any;
use Moose::Role;
use Moose::Util::TypeConstraints;
subtype '_PathClassDir', as 'Path::Class::Dir';
coerce '_PathClassDir',  from 'ArrayRef',
  via { Path::Class::Dir->new( @{ $_[0] } ) };
coerce '_PathClassDir', from 'Str', via { Path::Class::Dir->new( $_[0] ) };
subtype '_PathClassFile', as 'Path::Class::File';
coerce '_PathClassFile',  from 'ArrayRef',
  via { Path::Class::File->new( @{ $_[0] } ) };
coerce '_PathClassFile', from 'Str', via { Path::Class::File->new( $_[0] ) };

has 'name' => ( is => 'rw', isa => 'Str',);
has 'config_cache' => (
	is      => 'ro',
	isa     => 'HashRef',
	clearer => 'clear_config_cache',
	default => sub { {} }
);
has 'config_base_path' =>
  ( is => 'rw', lazy_build => 1, isa => '_PathClassDir', coerce => 1 );

has 'config_base_file' =>
  ( is => 'rw', lazy_build => 1, isa => '_PathClassFile', coerce => 1 );


sub load_config_file {
	my ( $self, $file ) = @_;
	my $config;
	unless ( $config = $self->config_cache->{ $file . "" } ) {
		die "Neither __PACKAGE__->config->{path} nor " . $file . " exist."
		  unless ( -e $file );
		$config = Config::Any->load_files(
			{
				files       => [$file],
				use_ext     => 1,
				driver_args => { General => { -UTF8 => 1 }, },
			}
		);
		( undef, $config ) = %{ $config->[0] };
		$self->config_cache->{ $file . "" } = $config;
	}
	return $config;
}

sub clear_config_cache {
	my ($self) = @_;
	#delete $self->config_cache;
}

1;
