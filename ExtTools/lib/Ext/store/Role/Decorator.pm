package Ext::store::Role::Decorator;
use Moose::Role;
use Carp qw/croak/;


sub render {
    my $self = shift;
    return "new Ext.FormPanel(" . js_dumper( $self->_render(@_) ) . ");";
}

sub _render {
    my $self  = shift;
    use Data::Dumper;
    my %param;
    if(ref $_[0] eq "HASH") {
        %param = %{$_[0]};
    } else {
        %param = @_;
    }
    my $return = {
        $self->action ? ( url => $self->action ) : (),
        items   => $self->_render_items,
        buttons => $self->_render_buttons,
        baseParams => {'x-requested-by' => 'ExtJS'},
        %param
    };
    my %attrs = $self->_get_attributes($self);
    return { %$return, %attrs };
    
}



sub _render_item {
    my $self = shift;
    my $element = shift || $self;

    my $class = ext_class_of( $element );
    return $class->render($element);
}

sub _render_items {
    my $self   = shift;
    my $from   = shift || $self;
    my $output = [];
    foreach my $element ( @{ $from->get_elements() } ) {
        next
          if ( !$element 
            || $element->type eq "Submit"
            || $element->type eq "Button"
            || $element->type eq "Reset"
            || $element->type eq "ContentButton" );

        # omit rendering field with 'omit_rendering' flag
        next
            if( defined $element->attributes()->{omit_rendering} && $element->attributes()->{omit_rendering} );
        push( @{$output}, $self->_render_item( $element ) );
    }

    # remove undef elements
    $output = [ grep { defined $_ } @{$output} ];
    return $output;
}

sub render_items {
    return js_dumper( shift->_render_items );
}


sub render_buttons {
    my $self = shift;
    return js_dumper( $self->_render_buttons );
}

sub _render_buttons {
    my $self   = shift;
    my $from   = shift || $self;
    my $output = [];
    foreach my $element ( @{ $from->get_all_elements() } ) {
        next
          unless ( $element->type eq "Submit"
            || $element->type eq "Button"
            || $element->type eq "ContentButton"
            || $element->type eq "Reset" );
        my $class = ext_class_of( $element );
        push( @{$output}, $class->render($element) );
    }
    return $output;
}

# Altlasten
sub ext_items {
    my $self = shift;
    my @items;
    my %map_types = (
        Text     => "textfield",
        Checkbox => "checkbox",
        Textarea => "textarea",
        Date     => "datefield",
        Hidden   => "hidden",
        Radio    => "radio",
        Select   => "combo",
        Fieldset => "fieldset",
    );
    for ( @{ $self->get_elements() } ) {
        next if ( $_->type eq "Submit" || $_->type eq "Button" );
        tie my %obj, 'Tie::Hash::Indexed';
        if ( $_->type eq "Fieldset" ) {
            %obj =
              ( items => \ext_items($_), title => $_->legend, autoHeight => 1 );
        } elsif ( $_->type eq "SimpleTable" ) {
            my @tr = grep { $_->tag eq "tr" } @{ $_->get_elements() };
            my @items;
            push( @items, ext_items($_) ) for (@tr);

            #die Dumper(@items);
            %obj = ( layout => "column", items => \( join( ",", @items ) ) );
        } elsif ( $_->type eq "Block" ) {
            if ( $_->tag eq "tr" ) {
                return ext_items($_);
            } elsif ( $_->tag eq "td" ) {
                %obj = (
                    columnWidth => 0.5,
                    layout      => 'form',
                    items       => \ext_items($_)
                );
            }
        } elsif ( $_->type eq "Repeatable" ) {
            return ext_items($_);
        } elsif ( $_->type eq "Checkbox" ) {
            %obj = (
                hideLabel => 1,
                boxLabel  => $_->label,
                $_->default ? ( inputValue => $_->default ) : ()
            );
        } elsif ( $_->type eq "Blank" ) {
            %obj = ( html => $_->name );
        } elsif ( $_->type eq "Select" ) {
            my $data;
            foreach my $option ( @{ $_->_options } ) {
                push( @{$data}, [ $option->{value}, $option->{label} ] );
            }
            my $string =
              js_dumper( { fields => [ "value", "text" ], data => $data } );
            %obj = (
                emptyText      => $_->label,
                mode           => "local",
                editable       => \0,
                displayField   => "text",
                valueField     => "value",
                hiddenName     => $_->name,
                autoWidth      => \0,
                forceSelection => \1,
                triggerAction  => "all",
                store => \( "new Ext.data.SimpleStore(" . $string . ")" )
            );
        }
        my $parent = $self->can("_get_attributes") ? $self : $self->form;
        %obj = (
            %obj,
            $_->can("label") ? ( fieldLabel => $_->label ) : (),
            $_->name         ? ( name       => $_->name )  : (),
            ( $_->can("default") && $_->default )
            ? ( value => $_->default )
            : (),
            $parent->_get_attributes($_)
        );
        $obj{xtype} = $map_types{ $_->type } if ( $map_types{ $_->type } );
        my $string = js_dumper( \%obj );
        utf8::decode($string);
        push( @items, \$string );
    }
    return js_dumper( \@items );
}

sub _get_attributes {
    my ( $self, $source ) = @_;
    my $obj = {};
    my @keys = keys %{ $source->attrs };
    for my $key (@keys) {
        my $type = ref( $source->attrs->{$key} );
        if ($type eq "HTML::FormFu::Literal") {
            $obj->{$key} = "" . $source->attrs->{$key};
        } else {
            $obj->{$key} = $source->attrs->{$key};
        }
    }
    
    return %{$obj};
}

sub ext_columns {
    my $self = shift;
    return _ext_columns($self);
}

sub _ext_columns {
    my $field = shift;
    my @return;
    my @childs =
      grep { $_->type() !~ /submit/i && $_->can("name") && defined $_->name }
      @{ $field->get_all_elements() };
    return \@childs;
}


*ext_validation = \&validation_response;

sub validation_response {
    my $form = shift;
    $form->model('HashRef')->flatten(0);
    $form->model('HashRef')->options(1);
    $form->set_options(shift);
    if ( $form->submitted_and_valid ) {
        my $return = { success => \1 };
        $form->default_values($form->params);
        $return->{data} = $form->model('HashRef')->create;
        return $return;
    } elsif ( $form->has_errors ) {
        my $return = { success => \0 };
        for ( @{ $form->get_errors } ) {
            push(
                @{ $return->{errors} },
                { id => $_->name, msg => $_->message }
            );
        }
        return $return;
    }
    return {};
}

sub set_options {
    my $self = shift;
    my $options = shift;
    while(my($k,$v) = each %{$options}) {
        $self->model('HashRef')->$k($v);
    }
}

sub grid_data {
    my $self   = shift;
    my $data = shift;
    my $param  = shift;
    
    croak 'First parameter has to be an array ref' unless(ref $data eq "ARRAY");
    
    my $rows = [];
    
    
    if(blessed $data->[0]) {
    
        $self->model('HashRef')->flatten(0);
        $self->model('HashRef')->options(1);
        $self->set_options(shift);
        $rows   = $self->ext_grid_data($data);
    
    } elsif(ref $data->[0] eq "HASH") {
        $rows   = $self->hashref_grid_data($data);
    } elsif($data->[0]) {
        croak 'Elements have to be a hash ref or blessed objects' . (ref $data->[0]);
    }
    
    my $return = {
        results  => scalar @{$rows},
        rows     => $rows,
        metaData => {
            totalProperty => 'results',
            root          => 'rows',
            idProperty => 'id',
            fields        => $self->_record
        }
    };
    return merge $return, $param;
}


sub hashref_grid_data {
    #carp 'Mia sann im Hashref';
    my $self = shift;
    my $data = shift;
    my @return;
    my @all_elements = @{ $self->get_all_elements() };
    my ( %element_cache, %deflator_cache, %inflator_cache, %options_cache );
    foreach my $datum ( @{$data} ) {
        my $obj;
        foreach my $column (@all_elements) {
            next if ( $column->type =~ /submit/i );
            my $name    = $column->name;
            next unless($name);
            my $element = $element_cache{$name}
              || $self->get_all_element($name);
            $element_cache{$name} ||= $element;
            next unless ($element);
            $obj->{$name} = $datum->{$name};

            my $inflators = $inflator_cache{$name}
              || $element->get_inflators;
            $inflator_cache{$name} ||= $inflators;

            foreach my $inflator ( @{$inflators} ) {
                $obj->{$name} = $inflator->inflator( $obj->{$name} );
            }

            my $deflators = $deflator_cache{$name}
              || $element->get_deflators;
            $deflator_cache{$name} ||= $deflators;

            foreach my $deflator ( @{$deflators} ) {
                $obj->{$name} = $deflator->deflator( $obj->{$name} );
            }

            if(ref $obj->{$name} || blessed $obj->{$name}) {
                carp "$name was set to undef because it was either blessed or a reference";
                $obj->{$name} = undef;
                next;
            }

            my $can_options = $options_cache{$name}
              || $element->can("_options");
            $options_cache{$name} ||= $element->can("_options");
            if ($can_options) {
                my @options = @{ $element->_options };
                my @option = grep { $_->{value} eq $obj->{$name} } @options;
                unless(@option) {
                    @options = map { @{$_->{group} || []} } @options;
                    @option = grep { $_->{value} eq $obj->{$name} } @options ;
                }
                $obj->{$name} = { label => join(", ", map { $_->{label} } @option), value => $obj->{$name}};
            }

            #if($column->type eq "Checkbox") {
            #    $obj->{$name} = \1 if($obj->{$name});
            #}
        }
        push( @return, $obj );
    }
    return \@return;
}

sub form_data {
    my $self = shift;
    my $data = shift;
    $self->model('HashRef')->flatten(0);
    $self->model('HashRef')->options(0);
    $self->set_options(shift);
    return {success => \0} unless($data);
    $self->model->default_values($data);
    return {success => \1, data => $self->model('HashRef')->create};
}

sub ext_grid_data {
    my $self = shift;
    my $data = shift;
    
    my @return;
        
    
    foreach my $datum ( @{$data} ) {
        $self->model('HashRef')->default_values({});
        $self->model->default_values($datum);
        push(@return, $self->model('HashRef')->create);
    }
    
    return \@return;
    
}


sub column_model {
    return "new Ext.grid.ColumnModel(" . js_dumper ( shift->_column_model(@_) ) . ");";
}

sub _column_model {
    my $form = shift;
    my @add  = @_;
    my $data;
    for my $element ( @{ $form->ext_columns() } ) {
        my $class = ext_class_of( $element );
        push( @{$data}, $class->column_model($element) ) if ( $class->can("record") );
    }

    for (@add) {
        push( @{$data}, $_ );
    }
    return $data;
}

sub record {
    return "Ext.data.Record.create(" . js_dumper( shift->_record(@_) ) . ");";
}

sub _record {
    my $form = shift;
    my @add  = @_;
    my $data;
    for my $element ( @{ $form->ext_columns() } ) {
        my $class = ext_class_of( $element );
        push( @{$data}, $class->record($element) ) if ( $class->can("record") );
    }

    for (@add) {
        push( @{$data}, $_ );
    }
    return $data;
}


1;