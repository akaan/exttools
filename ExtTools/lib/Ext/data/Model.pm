package Ext::data::Model;
use Moose;
use Data::Dumper;
use MooseX::Attribute::Chained;
use Carp qw/croak/;
extends 'Ext::data';
with 'Ext::Role::WriterAttributes', 'Ext::Role::TemplateAttributes';

has 'extend' => ( is => 'rw', isa => 'Str', default => 'Ext.data.Model' );
has 'model_format' =>
  ( is => 'ro', isa => 'Str', default => '%s.model.%sModel' );
has 'fields' =>
  ( is => 'rw', isa => 'ArrayRef[Ext::data::Field]', default => sub { [] } );
has 'associations' => (
	is      => 'rw',
	isa     => 'ArrayRef[Ext::data::Association]',
	default => sub { [] }
);

our %MAPPING = ( 'filter' => 'hasOne', 'multi' => 'hasMany' );

sub _build_template {
	return 'model.template';
}

our @structure = qw/fields associations/;

sub render {
	my ( $self, $resultset ) = @_;
	$self->app->log->info( $self->get_template() );
	my $class = $self->name;
	$class =~ s/.+::([\w_]+)$/$1/;
	my $app = ref($self->app);
	$app =~  s/.+::([\w_]+)$/$1/;

	my %vars = ( class => $class, app => $app  );
	foreach (@structure) {
		$vars{$_} = $self->$_ if scalar @{ $self->$_ };
	}
	#print "vars", Dumper( \%vars ), $self->template_name;
	$self->process( \%vars, $self->get_target_file('model') );
}

sub populate {
	my ( $self, $model_resultset ) = @_;
	my @cols         = $model_resultset->columns;
	my @foreign_keys = $model_resultset->relationships();

	# fields
	foreach (@cols) {
		push @{ $self->fields }, Ext::data::Field->new( name => $_, text => ucfirst(lc $_) );
	}

	# associations -> relationships
	foreach (@foreign_keys) {
		my $info         = $model_resultset->relationship_info($_);
		my ($foreignKey) = keys %{ $info->{cond} };
		my ($localKey)   = $info->{'cond'}->{$foreignKey};
		$localKey   =~ s/.+\.([\w_]+)$/$1/;
		$foreignKey =~ s/.+\.([\w_]+)$/$1/;
		my $type  = 'belongsTo';
		my $app   = ref( $self->app );
		my $class = $info->{'class'};
		$class =~ s/.+::([\w_]+)$/$1/;
		$type = $MAPPING{ $info->{'attrs'}->{'accessor'} }
		  if $MAPPING{ $info->{'attrs'}->{'accessor'} };
		push @{ $self->associations },
		  Ext::data::Association->new(
			name       => $_,
			foreignKey => $foreignKey,
			primaryKey => $localKey,
			type       => $type,
			model      => sprintf $self->model_format,
			$app, $class,
		  );
	}
}

1;
package Ext::data::Form;
use Moose;
extends "Ext::data::Model";

sub _build_template {
	return 'form.template';
}

1;

package Ext::data::Field;
use Moose;
extends "Ext::data";

#with 'Ext::Role::TemplateAttributes';

has 'name' => ( is => 'rw', isa => 'Str' );
has 'type' => ( is => 'rw', isa => 'Str', default => 'text' );
has 'text' => ( is => 'rw', isa => 'Str' );

sub _build_template {
	return 'field.template';
}

1;

package Ext::data::Association;
use Moose;
extends "Ext::data";

#with 'Ext::Role::TemplateAttributes';

has 'name'       => ( is => 'rw', isa => 'Str' );
has 'model'      => ( is => 'rw', isa => 'Str' );
has 'foreignKey' => ( is => 'rw', isa => 'Str' );
has 'primaryKey' => ( is => 'rw', isa => 'Str' );
has 'type'       => (
	is      => 'rw',
	default => sub { ( 'hasOne', 'hasMany', 'belongsTo' )[0] }
);

sub _build_template {
	return 'association.template';
}

1;
