#
# This file is part of CatalystX-ExtJS-Direct
#
# This software is Copyright (c) 2011 by Moritz Onken.
#
# This is free software, licensed under:
#
#   The (three-clause) BSD License
#
package Ext::direct::Action::Direct;
BEGIN {
  $Ext::direct::Action::Direct::VERSION = '2.1.4';
}
# ABSTRACT: Placeholder
use Moose;
extends qw(Catalyst::Action);

1;
__END__
=pod

=head1 NAME

Ext::direct::Action::Direct - Placeholder

=head1 VERSION

version 2.1.4

=head1 AUTHOR

Moritz Onken <onken@netcubed.de>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2011 by Moritz Onken.

This is free software, licensed under:

  The (three-clause) BSD License

=cut

