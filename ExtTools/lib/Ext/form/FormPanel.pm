package Ext::form::FormPanel;
use Moose;
with 'Ext::controller::Role::HasModel';

#use JavaScript::Dumper;
use Data::Dumper;
use Ext::form::Element::TextField;
use Ext::form::Element::ComboBox;

has 'model' => ( is=>'rw');

for my $name ( qw(
    items
    name
    app
    title
    columns
    template
    ) )
{
    has $name => (
        is      => 'rw',
        isa     => 'Str',
        lazy => 1,
        default => ''
    );
}


sub BUILD {
	my ($self) = @_;
	$self->template('formpanel.tt2');
    my $formstring = "";
    my $columnDef = "";
    foreach my $e (@{$self->model->elements}) {
        my $f;
        if ($e->type eq 'string') {
            $f = newTextField($e);
            $formstring .= $f->render();
        } elsif ($e->type eq 'combo') {
        	$f = newComboBox($e);
        	$formstring .= $f->render();
        }
        
        my $cd = $e->columnDef;
        $columnDef .= $cd->render() if $cd;
    }
    $self->items($formstring);	
    $self->columns($columnDef);
}

sub newTextField {
	my ($element) = @_;
	return Ext::form::Element::TextField->new('element'=>$element);
}
sub newComboBox {
    my ($element) = @_;
    return Ext::form::Element::ComboBox->new('element'=>$element);
}


sub form_data {
	my ($self,$success, $method, $tid, @rest) = @_;
    my $return = {
        action=>$self->name,
        method=>$method ? $method : 'update',
        type=>'rpc',
        'tid'=>$tid ? $tid : 0,
        result => { 'success'=>$success ? $success : 0 }
    };
    print ref($self), ": form_returns: " . ref($return);
    return $return;
}

sub process {
	my ($self) = @_;
	
}

sub submitted_and_valid {
    return 1;	
}

sub validation_response {
}

1;