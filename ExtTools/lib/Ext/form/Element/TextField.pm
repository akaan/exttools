package Ext::form::Element::TextField;

use Moose;
with 'Ext::debug', 'Ext::form::Role::HasValue', 'Ext::form::Role::HasLabel',
  'Ext::form::Role::IsElement';

sub BUILD {
	my ( $self, $args ) = @_;
	$self->xtype('textfield');
	my $element = $args->{'element'};
	return unless $element;
	$self->name( $element->name );
	$self->fieldLabel( $element->label ? $element->label : $element->name );
}

sub render {
	my ($self)   = @_;
	my $string   = "{ \n%s\n },\n";
	my $template = "";
	foreach my $k (qw/xtype name fieldLabel allowBlank/) {
		$template .= sprintf "  '%s' : '%s', \n", $k, $self->$k ? $self->$k : ''
		  if $self->$k;
	}
	return sprintf $string, $template;
}

1;
