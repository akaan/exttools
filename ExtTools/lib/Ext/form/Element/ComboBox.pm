package Ext::form::Element::ComboBox;

use Moose;
with 'Ext::debug', 'Ext::form::Role::HasValue', 'Ext::form::Role::HasLabel',
  'Ext::form::Role::IsElement';
  
has 'store' => ( is=>'rw', isa=>'Str');
has 'valueField' => (is=>'rw', isa=>'Str');

sub BUILD {
    my ( $self, $args ) = @_;
    $self->xtype('combobox');
    my $element = $args->{'element'};
    return unless $element;
    $self->name( $element->name );
    $self->fieldLabel( $element->label ? $element->label : $element->name );
    $self->store( $element->store);
    $self->valueField( $element->name );
}

sub render {
    my ($self)   = @_;
    my $string   = "{ \n%s\n },\n";
    my $template = "";
    foreach my $k (qw/xtype name fieldLabel valueField store/) {
        $template .= sprintf "  '%s' : '%s', \n", $k, $self->$k ? $self->$k : ''
          if $self->$k;
    }
    return sprintf $string, $template;
}

1;
