package Ext::form::Role::HasLabel;
use Moose::Role;

has 'hideLabel' => ( is => 'rw', isa => 'Str', default => 'false' );
has 'fieldLabel' => ( is => 'rw', isa => 'Str' );
has 'labelWidth' => ( is=>'rw', isa=>'Number');


1;