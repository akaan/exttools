package Ext::form::Role::HasValue;
use Moose::Role;

has 'value' => ( is => 'rw', isa => 'Str');
has 'readOnly' => ( is => 'rw', isa => 'Str' );
has 'width' => ( is => 'rw', isa => 'Number');
has 'allowBlank' => ( is=>'rw', isa=>'Str');
has 'disabled' => ( is=>'rw', isa=>'Srt');

1;