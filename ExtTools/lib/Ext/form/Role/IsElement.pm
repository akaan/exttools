package Ext::form::Role::IsElement;

use Moose::Role;

has 'xtype' => (is=>'rw', isa=>'Str', default=>'textfield');
has 'name' => (is=>'rw', isa=>'Str');

1;
