package Ext::form::Element;

use Moose;

has 'xtype' => ( is=>'ro', isa=>'Str', default=>'textfield');
has 'name' => (is=>'rw', isa=>'Str');

1;