package Ext::debug;
use Moose::Role;
use Scalar::Util qw(blessed);

sub Dumper {
	
	if ( blessed $_[0] ) {
        my $self = shift @_;
		my $d = Data::Dumper->new( \@_ );
		my $message =  $d->Dump;
		return sprintf "trace taken at %s: %s", ref($self), $message;
	}
	else {
		
		my $d = Data::Dumper->new( \@_ );
		return sprintf "trace taken at anon: %s", $d->Dump;
	}
}

1;
