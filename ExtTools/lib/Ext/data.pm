package Ext::data;
use Moose;
use Carp qw( croak );
with 'Ext::Role::NameAttributes';

has 'app' => ( is => 'rw' );

1;
