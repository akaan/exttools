package Catalyst::Helper::ExtJS;

use Data::Dumper;
use YAML;
use Module::Finder;
use Moose;
use Carp qw/croak/;

require Exporter;
our @ISA    = qw( Exporter );
our @EXPORT = qw( APP_ROOT BASE);

use constant APP_ROOT  => 'root/static';
use constant BASE      => 'root';
use constant FORM_ROOT => 'root/forms';
use constant VIEW_ROOT => 'root/static/view';

my %TEMPLATES;
__PACKAGE__->meta->make_immutable;

# Do not touch this method, *EVER*, it is needed for back compat.
## addendum: we had to split this method so we could have backwards
## compatability.  otherwise, we'd have no way to pass stuff from __DATA__

sub find_module {
  my ( $self, $module ) = @_;

  # +----
  my $finder = Module::Finder->new( dirs => ['./lib'] );
  my ($schema)     = grep { /^Schema::\w+$/ } $finder->modules;
  my ($module_ref) = grep { /::Model::\w+$/ } $finder->modules;

  # +----
  return ( $schema, $module_ref );
}

sub load_modules {
	my ( $self, $arrlist ) = @_;
	return unless $arrlist;
	foreach my $pm (@$arrlist) {
		print "loading $pm.\n";
		eval "require $pm";
		if ($@) {
			print Dumper($arrlist);
			Catalyst::Exception->throw( message =>
qq/Couldn't load base classes "$pm" for resolving ($pm), "$@"\n/
			);
		}
	}
}

sub render_file {
  my ( $self, $helper, $file, $path, $vars, $perms ) = @_;
  my $class = ref($self);

  #die Dumper($file);
  my $template =
    $TEMPLATES{ $file
    };    #$self->get_file( Catalyst::Helper::ExtJS->BASE, $file );
          #print Dumper($template);
  my $template_string = $template;    #join( "\n", @$template );
  $path = sprintf "%s/%s", $self->{'APP_ROOT'}, $file unless $path;

  if ( -e $path and not $vars->{'overwrite'} ) {
    $path .= ".new";
  }
  $self->log(
           sprintf( "Generating '%s' based on template '%s'.", $path, $file ) );
  if ( -f $path and $vars->{'overwrite'} ) {
    system("rm $path");
  }

  #print "CONTENT" . Dumper($template);
  $helper->render_file_contents( $template_string, $path, $vars, $perms );
}

sub get_file {
  my ( $self, $template_base, $file_name ) = @_;

  #print Dumper(\@_);
  if ( !-d $template_base ) {
    Catalyst::Exception->throw( message =>
qq/The root forms directory $template_base under application root is missing./
    );
  }
  my $template_name_file = sprintf "%s/%s", $template_base, $file_name;
  if ( !-f $template_name_file ) {
    croak "Error: unable to read template file from $template_name_file";
  }
  open XFILE, "< $template_name_file"
    or croak "error: reading file $template_name_file: $!\n";
  my @content = ();
  while (<XFILE>) {
    chomp;
    push @content, $_;
  }
  close(XFILE);
  return \@content;
}

sub write_javascript_class {
  my ( $self, $template, $app, $overwrite ) = @_;
  my $class         = ref($self);
  my $template_base = Catalyst::Helper::ExtJS->BASE;
  if ( !-d $template_base ) {
    Catalyst::Exception->throw( message =>
qq/The root forms directory $template_base under application root is missing./
    );
  }
  my $template_name_file = sprintf "%s/%s.yml", $template_base, lc $app;
  if ( -e $template_name_file and not $overwrite ) {
    $template_name_file .= '.new';
  }
  $self->log( sprintf( "writing '%s' with form data.", $template_name_file ) );
  open FILE, "> $template_name_file" or die "$!";
  print FILE Dump($app);
  close FILE;
}

sub save_yaml {
  my ( $self, $yml, $module, $overwrite ) = @_;
  my $formbase = './root/forms/';
  if ( ! defined $module ) {
    Catalyst::Exception->throw( message =>
       qq/The module name is not defined.\n/
    );
  }
  if ( !-d $formbase ) {
    Catalyst::Exception->throw( message =>
       qq/The root forms directory $formbase under application root is missing./
    );
  }
  my $form_name_file = sprintf "%s/%s.yml", $formbase, lc $module;
  if ( -e $form_name_file and not $overwrite ) {
    $form_name_file .= '.new';
  }
  $self->log( sprintf( "writing '%s' with form data.", $form_name_file ) );
  open FILE, "> $form_name_file" or die "Error writing $form_name_file: $!";
  print FILE Dump($yml);
  close FILE;
}

sub log {
  my ( $self, $message, $level ) = @_;
  $level = 'info' unless $level;
  printf "[%5s] %s\n", $level, $message;
}

%TEMPLATES = (
  'app.template' => qq{
Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', './static/extjs/examples/ux/');
Ext.require([
    'Ext.*',
    'Ext.direct.*',
    'Ext.app.*',
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.toolbar.Paging',
    'Ext.ModelManager',
    'Ext.ux.grid.FiltersFeature',
    'Ext.tip.QuickTipManager',
]);
/*
 * BUG: suspentEvents not honoured in Ext.app.EventBus
 *
 * note: this fix does not queue events when asked.
 *
 * http://www.sencha.com/forum/showthread.php?171525
 */
Ext.syncRequire('Ext.app.EventBus');
Ext.override(Ext.app.EventBus, {
    constructor: function() {
        this.mixins.observable.constructor.call(this);
        this.bus = {};
        var me = this;
        Ext.override(Ext.Component, {
            fireEvent: function(ev) {
                if (Ext.util.Observable.prototype.fireEvent.apply(this, arguments) !== false && !this.eventsSuspended) {
                    return me.dispatch.call(me, ev, this, arguments);
                }
                return false;
            }
        });
    }
});
Ext.util.Format.comboboxRenderer = function(combo,value) {
        var record = combo.findRecord(combo.valueField,value);
        return record ? record.get(combo.displayField) : combo.valueNotFoundText;
};
Ext.direct.Manager.addProvider(Ext.app.REMOTING_API);
Ext.application({
    name: '[% name %]',
    controllers: [
[% FOREACH c IN controllers %]       '[% c %]',
[% END %]    ],
    appFolder: 'static',
    autoCreateViewport: true,
    launch : function () {
[% FOREACH c IN controllers %]       Ext.getStore('[% c %]').load();
[% END %]    ]
    }
});
        
},
  'controller.template' => qq{
Ext.define('[% name %].controller.[% class %]Controller', {
    extend: 'Ext.app.Controller',
    requires: [
               '[% name %].model.[% class %]Model',
               '[% name %].store.[% class %]Store'
           ],
    models: ['[% class %]Model'],
    stores: ['[% class %]Store'],
    views: [ 'Form'],
    init: function () {
    }
});
},
  'form.template' => qq{---
elements:
[% FOREACH i IN fields %]   - label: [% i.text %]
     name: [% i.name %]
[% END %]
model_config:
     resultset: [% class %]
     schema: BMC
},
  'model.template' => qq{  // Set up a model to use in our Store
Ext.define('[% app %].model.[% class %]Model',{
   extend: 'Ext.data.Model',
   fields: [ 
[% FOREACH i IN fields %]       { name: '[% i.name %]', type: '[% i.type %]' }[%-  ", " IF NOT loop.last %]
[% END %]
    ]
[% IF associations %]   ,associations : [ [% END %]    
[% FOREACH i IN associations %]       { name: '[% i.name %]', type: '[% i.type %]', 
           primaryKey : '[% i.primaryKey %]', foreignKey : '[% i.foreignKey %]', 
           model: '[% i.model %]', },
[% END %][% IF associations %]   ][%-  ", " IF NOT loop.last %][% END %]    
});

/* Possible column definition added for bonus 

   columns: [ 
[% FOREACH i IN fields %]       { id: '[% i.name %]', dataIndex: '[% i.name %]', header: '[% i.name %]'  }[%-  ", " IF NOT loop.last %]
[% END %]
    ],

*/
  },
  'store.template' =>
    qq{// Typical Store collecting the Proxy, Reader and Writer together.
Ext.define('[% name %].store.[% class %]Store', {
    extend : 'Ext.data.Store',
    model : '[% name %].model.[% class %]Model',
    constructor : function(config) {
        var configuration = Ext.data.Store.Definition({
            // remote : true,
            name : '[% class %]',
            url : '/json/[% class %]Store.json'
        });
        Ext.apply(config, configuration);
        this.callParent([ config ]);
    },
    autoSave : true
});
    
},
  'Viewport.template' => qq{/**
 * The main application viewport, which displays the whole application
 * \@extends Ext.Viewport
 */
Ext.define('[% name %].view.Viewport', {
    extend: 'Ext.Viewport',    
    
    layout: 'fit',
    
    requires: [
/* '[% name %].view.screen.MyGrid', */ 
    ],
    
    initComponent: function() {
        var me = this;
        
        Ext.apply(me, {
            items: [
                {
                }
            ]
        });
        me.callParent(arguments);
    }
});
    
},
  'index.template' => qq{
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
  <title>Ext JS 4 MVC Examples - [% name %].com | Sencha Try</title>
    <link rel="stylesheet" type="text/css" href="./extjs/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="./[% name %].css"/>
    <script type="text/javascript" charset="utf-8" src="./extjs/ext-all-debug.js"/>
    <script type="text/javascript" src="./api/src"></script>
    
    <!-- App Files -->
    <script type="text/javascript" src="./static/app.js"></script>
    
</head>
<body>
<div id="container"></div>
</body>
</html>
 },
  'combo.template' => qq{
Ext.define('[% name %].view.[% base %].[% class %]Combo', {
    extend : 'Ext.form.ComboBox',
    alias : 'widget.[% class FILTER lower %]combo',
    store : '[% class %]Store',
    name : '[% pk %]',
    displayField : '[% pk %]',
    valueField : '[% pk %]',
    fieldLabel : '[% class FILTER ucfirst %]',
    triggerAction: 'all',
    //editable: false,
    queryMode : 'remote',
    //loadMask: true,
    typeAhead: true,
    minChars : 2,
    //forceselection: true,   
    tpl: Ext.create('Ext.XTemplate',
        '<tpl for=".">',
            '<div class="x-boundlist-item">{[% pk %]}</div>',
        '</tpl>'
    ),
});    	
    }
);
1;
