package Catalyst::Helper::Controller::ExtJS::Forms;
use Moose;
extends qw/Catalyst::Helper::ExtJS/;

use Data::Dumper;
use Module::Finder;

sub mk_stuff {
	my ( $self, $helper, $_schema, $module, $overwrite ) = @_;
	
	
    my ($schema, $module_ref) = $self->find_module($module);
	$self->log("processing $module ($schema)");
	unless ($module_ref) {
		Catalyst::Exception->throw( message =>
			  qq/Couldn't base "$module_ref" ($module_ref), "$@"/
		);
	}
		my ($model_name) = $helper->{name};
	$self->log( sprintf( "looking for %s module.", $model_name ) );
	next unless $model_name;
	
	$self->log("(re)loading ref($schema) $module_ref for '$model_name'" . ref($schema));
	eval "require $schema;"  unless $schema->can('connect');

	my $model_resultset = $schema->resultset($model_name);
	my @cols            = $model_resultset->result_source->columns;
	my @foreign_keys    = $model_resultset->result_source->relationships();
	my ($schema_core) = $schema =~ m/.+::(\w+)$/xi;
  $model_name =~ s/.+Model::(\w+)$/$1/xi; # if $model_name =~ /Model::/;
  $module =~ s/.+Model::(\w+)$/$1/xi; # if $model_name =~ /Model::/;
	my %yml = (
		'store' => {
			'resultset' => $model_name,  
			'schema'    => $schema_core,
		},
		'model' => $module
	);
	foreach my $column (@cols) {
		push @{ $yml{'elements'} }, { 'name' => $column };
	}
	$self->save_yaml( \%yml, $model_name, $overwrite );
	return 1;
}

sub mk_compclass {
	my ( $self, $helper, $overwrite, @options ) = @_;
	my $module = $helper->{name};
	my ( $schema, $model ) = $self->find_module($module);
	$self->mk_stuff( $helper, $schema, $model, $overwrite, @options );
}

#sub mk_stuff {
#	my ( $self, $helper, $module, $overwrite, @options ) = @_;
#
#	# +----
##	my $finder = Module::Finder->new( dirs => ['./lib'] );
##	my ($schema)     = grep { /^Schema::\w+$/ } $finder->modules;
##	my ($module_ref) = grep { qr/\Q$module\E$/ } $finder->modules;
##    # +----
#    my ($schema, $module_ref) = $self->find_module($module);
#	$self->log("processing $module ($schema)");
#	unless ($module_ref) {
#		Catalyst::Exception->throw( message =>
#			  qq/Couldn't base "$module_ref" ($module_ref), "$@"/
#		);
#	}
#	else {
#        foreach ( Catalyst::Helper::ExtJS->FORM_ROOT,Catalyst::Helper::ExtJS->APP_ROOT,Catalyst::Helper::ExtJS->BASE ) {
#            $helper->mk_dir($_);
#        }
#		$self->log("making directory structures");
#		my @dirs = qw/model store controller view combo/;
#		foreach ( map { sprintf "%s/%s",Catalyst::Helper::ExtJS->APP_ROOT, $_ } @dirs ) {
#			$helper->mk_dir($_);
#		}
#
#		# forms
#		$self->log("connecting to module $module");
#        $self->load_modules([$schema,$module_ref]);
#        $self->log("connecting to database with $schema");
#		$schema->connect();
#		my @helpers =
#		  qw/Catalyst::Helper::Controller::ExtJS::Forms Catalyst::Helper::Controller::ExtJS::Views/;
#		foreach my $help_class (@helpers) {
#			eval "require $help_class";
#			if ($@) {
#				Catalyst::Exception->throw(
#					message => qq/Couldn't load helper "$help_class", "$@"/ );
#			}
#			$self->log(">>>>executing helper class $help_class");
#			$help_class->mk_stuff( $helper, $schema, $module_ref, $module, $overwrite,
#				@options );
#		}
#		$self->log("done");
#		return;
#	}
#}
#
#sub mk_compclass {
#	my ( $self, $helper, $overwrite, @options ) = @_;
#	my $file = $helper->{file};
#	$helper->render_file( 'compclass', $file );
#	if ($file) {
#		$self->mk_stuff( $helper, $file, $overwrite, @options );
#	}
#}

__DATA__
__compclass__
package [% class %];

use Moose;
extends 'Ext::controller::REST';


1;

__END__
1;
