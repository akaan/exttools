package Catalyst::Helper::Controller::ExtJS::AppSkeleton;
use Moose;
extends qw/Catalyst::Helper::ExtJS/;

use Data::Dumper;
use YAML;

#__PACKAGE__->config(
#	form_dir => 'root/forms',
#);

=head1 NAME

Catalyst::Helper::Controller::API - Helper for CatalystX::Controller::ExtJS::Direct::API Views

=head1 SYNOPSIS

    script/create.pl controller API API::ExtJS

=head1 DESCRIPTION

Helper for API::ExtJS Views.

=head2 METHODS

=head3 mk_compclass

=cut

sub mk_stuff {
	my ( $self, $helper, $module, $overwrite ) = @_;
	my ($model_name) = $helper->{name};
	my %vars = (
		name      => $helper->{'app'},
		overwrite => $overwrite,
		class     => $model_name,

	);
	my @templates = (
		{
			template => 'app.template',
			name => 'app.js',
			dir      => Catalyst::Helper::ExtJS->APP_ROOT
		},
		{
			template => 'Viewport.template',
			name => 'Viewport.js',
			dir      => Catalyst::Helper::ExtJS->VIEW_ROOT
		},
        {
            template => 'index.template',
            name => 'index',
            dir      => Catalyst::Helper::ExtJS->BASE . '/src'
        }
	);
	foreach my $t (@templates) {
        $helper->mk_dir($t->{'dir'}) if $t->{'dir'};
		my $base_name = $t->{'template'};
		$base_name =~ s/\.template$//g;
		my $capitalized = ucfirst($base_name);
		$vars{'fname'} = sprintf "%s/%s",
		  $t->{'dir'},
		  $t->{'name'} ? $t->{'name'} : $base_name . ".js" ,
		  $model_name, $t =~ /controller/ ? '' : $capitalized;
		$self->log(
			"ExtJS4 App Skeleton Generator for $model_name targeted to $vars{'fname'}.");

		$self->render_file( $helper, $t->{'template'}, $vars{'fname'}, \%vars );

	}
	$self->log("done");
	return 1;
}

sub mk_compclass {
    my ( $self, $helper, $overwrite, @options ) = @_;
    my $file = $helper->{file};
    $helper->render_file( 'compclass', $file );
    if ($file) {
        $self->mk_stuff( $helper, $file, $overwrite, @options );
    }
}

__DATA__
__compclass__
package [% class %];

use Moose;
extends 'Ext::controller::REST';


1;
