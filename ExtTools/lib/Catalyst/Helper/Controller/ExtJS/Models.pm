package Catalyst::Helper::Controller::ExtJS::Models;
use Moose;
extends qw/Catalyst::Helper::ExtJS/;

use Data::Dumper;
use YAML;

#__PACKAGE__->config(
#	form_dir => 'root/forms',
#);

=head1 NAME

Catalyst::Helper::Controller::API - Helper for CatalystX::Controller::ExtJS::Direct::API Views

=head1 SYNOPSIS

    script/create.pl controller API API::ExtJS

=head1 DESCRIPTION

Helper for API::ExtJS Views.

=head2 METHODS

=head3 mk_compclass

=cut

sub mk_stuff {
	my ( $self, $helper, $schema, $module, $overwrite ) = @_;

	my ($model_name) = $helper->{name};
	$self->log( sprintf( "looking for %s module.", $model_name ) );
	next unless $model_name;
	my $model_resultset = $schema->resultset($model_name);
	my $name = $model_name;
	my $lname  = lc $name;
	my @cols   = $model_resultset->result_source->columns;
    my ( $pk, $too_much ) = $model_resultset->result_source->primary_columns;
	my @fields = ();
	$self->log( sprintf "generating %s as '%s' extjs component.",
		$name, $model_name );

	foreach my $column (@cols) {
		my ($n) =
		  $schema->resultset($model_name)->result_source->column_info($column);
		my $meta = "$n->{data_type}";
		$meta =~ s/(\w+)\s*.*$/$1/;
		$meta = 'string' if $meta eq '' or $meta eq 'character';
		push @fields, { name => $column, type => $meta };
	}

	#$target{'fields'} = \@fields;
	my %vars = (
		name        => $helper->{'app'},
#		controllers => [ $instance->controllers() ],
#		models      => [ $instance->models() ],
		fields      => \@fields,
		overwrite   => $overwrite,
		class       => $name,
		model => $model_name,
		pk => $pk,    
		schema => $schema,
	);
	my @templates =
	  ( 'model.template', 'store.template', 'controller.template', 'combo.template' );
	foreach my $t (@templates) {
		my $base_name = $t;
		$base_name =~ s/\.template$//g;
		my $capitalized = ucfirst($base_name);
		$vars{'capitalized'}=$capitalized;
		$vars{'base'}=$base_name;
		$vars{'fname'} = sprintf "%s/%s/%s%s.js",
		  Catalyst::Helper::ExtJS->APP_ROOT,
		  $base_name,
		  $name, $t =~ /controller/ ? '' : $capitalized;
		$self->log(
			"ExtJS4 Model Generator for $name targeted to $vars{'fname'}.");

		$self->render_file( $helper, $t, $vars{'fname'}, \%vars );

	}
	return 1;
}

=head1 SEE ALSO

L<CatalystX::Controller::ExtJS::Direct::API>, L<Template::Alloy>, L<Catalyst::Manual>

=head1 AUTHORS

Andre Kaan, C<bmc88@telfort.nl>

Based on the code of C<CatalystX::Controller::ExtJS::Direct::API>, by

Monkton, C<sri@oook.de>

=head1 LICENSE

This library is free software . You can redistribute it and/or modify
it under the same terms as perl itself.

=cut

1;
