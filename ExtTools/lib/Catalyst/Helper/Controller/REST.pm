package Catalyst::Helper::Controller::REST;

use Data::Dumper;
use strict;

=head1 NAME

Catalyst::Helper::Controller::API - Helper for CatalystX::Controller::ExtJS::Direct::API Views

=head1 SYNOPSIS

    script/create.pl controller API API::ExtJS

=head1 DESCRIPTION

Helper for API::ExtJS Views.

=head2 METHODS

=head3 mk_compclass

=cut

sub mk_compclass {
    my ( $self, $helper, @rest ) = @_;
    my $file = $helper->{file};
    $helper->render_file( 'compclass', $file );
    if (@rest) {
    	
    }
}

=head1 SEE ALSO

L<CatalystX::Controller::ExtJS::Direct::API>, L<Template::Alloy>, L<Catalyst::Manual>

=head1 AUTHORS

Andre Kaan, C<bmc88@telfort.nl>

Based on the code of C<CatalystX::Controller::ExtJS::Direct::API>, by

Monkton, C<sri@oook.de>

=head1 LICENSE

This library is free software . You can redistribute it and/or modify
it under the same terms as perl itself.

=cut

1;

__DATA__
__compclass__
package [% class %];

use Moose;
extends 'Ext::controller::REST';


1;

__END__
