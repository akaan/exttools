use warnings;
use lib "./lib";
use Ext::data::Model;
use BmcApp;
use Data::Dumper;

my $app = BmcApp->new();
my $name = 'BMC::BmcStyle';
my $appmodel = $app->model($name);
my $model = Ext::data::Model->new(name => $name);
$model->app($app);
#print Dumper($appmodel);
$model->populate($appmodel->result_source());
$model->render();


my $form = Ext::data::Form->new(name => $name);
$form->app($app);
$form->populate($appmodel->result_source());
$form->render();

1;