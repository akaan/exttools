use utf8;
package Schema::BMC::Result::AwsInventoryBase;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::BMC::Result::AwsInventoryBase

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<AWS_INVENTORY_BASE>

=cut

__PACKAGE__->table("AWS_INVENTORY_BASE");

=head1 ACCESSORS

=head2 sku

  data_type: 'text'
  is_nullable: 0

=head2 title

  data_type: 'text'
  is_nullable: 1

=head2 standard-product-id

  accessor: 'standard_product_id'
  data_type: 'text'
  is_nullable: 1

=head2 product-id-type

  accessor: 'product_id_type'
  data_type: 'text'
  is_nullable: 1

=head2 product_type

  data_type: 'text'
  is_nullable: 1

=head2 brand

  data_type: 'text'
  default_value: 'Godivas Secret Wigs'
  is_nullable: 1

=head2 manufacturer

  data_type: 'text'
  is_nullable: 1

=head2 mfr-part-number

  accessor: 'mfr_part_number'
  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 bullet-point1

  accessor: 'bullet_point1'
  data_type: 'text'
  is_nullable: 1

=head2 bullet-point2

  accessor: 'bullet_point2'
  data_type: 'text'
  is_nullable: 1

=head2 bullet-point3

  accessor: 'bullet_point3'
  data_type: 'text'
  is_nullable: 1

=head2 bullet-point4

  accessor: 'bullet_point4'
  data_type: 'text'
  is_nullable: 1

=head2 bullet-point5

  accessor: 'bullet_point5'
  data_type: 'text'
  is_nullable: 1

=head2 merchant-catalog-number

  accessor: 'merchant_catalog_number'
  data_type: 'text'
  is_nullable: 1

=head2 msrp

  data_type: 'text'
  is_nullable: 1

=head2 ingredients1

  data_type: 'text'
  is_nullable: 1

=head2 ingredients2

  data_type: 'text'
  is_nullable: 1

=head2 ingredients3

  data_type: 'text'
  is_nullable: 1

=head2 indications

  data_type: 'text'
  is_nullable: 1

=head2 directions

  data_type: 'text'
  is_nullable: 1

=head2 minimum-weight-recommendation

  accessor: 'minimum_weight_recommendation'
  data_type: 'text'
  is_nullable: 1

=head2 maximum-weight-recommendation

  accessor: 'maximum_weight_recommendation'
  data_type: 'text'
  is_nullable: 1

=head2 weight-recommendation-unit-of-measure

  accessor: 'weight_recommendation_unit_of_measure'
  data_type: 'text'
  is_nullable: 1

=head2 flavor

  data_type: 'text'
  is_nullable: 1

=head2 size

  data_type: 'text'
  default_value: 'adjustable'
  is_nullable: 1

=head2 color

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 1

=head2 scent

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "sku",
  { data_type => "text", is_nullable => 0 },
  "title",
  { data_type => "text", is_nullable => 1 },
  "standard-product-id",
  {
    accessor    => "standard_product_id",
    data_type   => "text",
    is_nullable => 1,
  },
  "product-id-type",
  { accessor => "product_id_type", data_type => "text", is_nullable => 1 },
  "product_type",
  { data_type => "text", is_nullable => 1 },
  "brand",
  {
    data_type     => "text",
    default_value => "Godivas Secret Wigs",
    is_nullable   => 1,
  },
  "manufacturer",
  { data_type => "text", is_nullable => 1 },
  "mfr-part-number",
  { accessor => "mfr_part_number", data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "bullet-point1",
  { accessor => "bullet_point1", data_type => "text", is_nullable => 1 },
  "bullet-point2",
  { accessor => "bullet_point2", data_type => "text", is_nullable => 1 },
  "bullet-point3",
  { accessor => "bullet_point3", data_type => "text", is_nullable => 1 },
  "bullet-point4",
  { accessor => "bullet_point4", data_type => "text", is_nullable => 1 },
  "bullet-point5",
  { accessor => "bullet_point5", data_type => "text", is_nullable => 1 },
  "merchant-catalog-number",
  {
    accessor    => "merchant_catalog_number",
    data_type   => "text",
    is_nullable => 1,
  },
  "msrp",
  { data_type => "text", is_nullable => 1 },
  "ingredients1",
  { data_type => "text", is_nullable => 1 },
  "ingredients2",
  { data_type => "text", is_nullable => 1 },
  "ingredients3",
  { data_type => "text", is_nullable => 1 },
  "indications",
  { data_type => "text", is_nullable => 1 },
  "directions",
  { data_type => "text", is_nullable => 1 },
  "minimum-weight-recommendation",
  {
    accessor    => "minimum_weight_recommendation",
    data_type   => "text",
    is_nullable => 1,
  },
  "maximum-weight-recommendation",
  {
    accessor    => "maximum_weight_recommendation",
    data_type   => "text",
    is_nullable => 1,
  },
  "weight-recommendation-unit-of-measure",
  {
    accessor    => "weight_recommendation_unit_of_measure",
    data_type   => "text",
    is_nullable => 1,
  },
  "flavor",
  { data_type => "text", is_nullable => 1 },
  "size",
  { data_type => "text", default_value => "adjustable", is_nullable => 1 },
  "color",
  { data_type => "text", is_foreign_key => 1, is_nullable => 1 },
  "scent",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</sku>

=back

=cut

__PACKAGE__->set_primary_key("sku");


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-05-27 11:12:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NASVVE/RkLicXLoDhaV2SQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
