 use utf8;
package Schema::BMC::Result::AwsInventoryFullLine;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::BMC::Result::AwsInventoryFullLine

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<aws_inventory_full_line>

=cut

__PACKAGE__->table("aws_inventory_full_line");

=head1 ACCESSORS

=head2 item_sku

  data_type: (empty string)
  is_nullable: 1

=head2 external_product_id

  data_type: 'integer'
  is_nullable: 1

=head2 external_product_id_type

  data_type: 'text'
  is_nullable: 1

=head2 item_name

  data_type: (empty string)
  is_nullable: 1

=head2 brand_name

  data_type: (empty string)
  is_nullable: 1

=head2 manufacturer

  data_type: (empty string)
  is_nullable: 1

=head2 part_number

  data_type: 'text'
  is_nullable: 1

=head2 product_description

  data_type: (empty string)
  is_nullable: 1

=head2 feed_product_type

  data_type: (empty string)
  is_nullable: 1

=head2 item_type

  data_type: (empty string)
  is_nullable: 1

=head2 update_delete

  data_type: (empty string)
  is_nullable: 1

=head2 quantity

  data_type: 'integer'
  is_nullable: 1

=head2 standard_price

  data_type: 'integer'
  is_nullable: 1

=head2 currency

  data_type: (empty string)
  is_nullable: 1

=head2 max_order_quantity

  data_type: (empty string)
  is_nullable: 1

=head2 fulfillment_latency

  data_type: (empty string)
  is_nullable: 1

=head2 ''

  accessor: '_'
  data_type: (empty string)
  is_nullable: 1

=head2 '':1

  accessor: '_1'
  data_type: (empty string)
  is_nullable: 1

=head2 p_quantity

  data_type: (empty string)
  is_nullable: 1

=head2 product_tax_code

  data_type: (empty string)
  is_nullable: 1

=head2 product_site_launch_date

  data_type: (empty string)
  is_nullable: 1

=head2 restock_date

  data_type: (empty string)
  is_nullable: 1

=head2 is_discontinued_by_manufacturer

  data_type: (empty string)
  is_nullable: 1

=head2 max_aggregate_ship_quantity

  data_type: (empty string)
  is_nullable: 1

=head2 product_tax_code:1

  accessor: 'product_tax_code_1'
  data_type: (empty string)
  is_nullable: 1

=head2 product_site_launch_date:1

  accessor: 'product_site_launch_date_1'
  data_type: (empty string)
  is_nullable: 1

=head2 merchant_release_date

  data_type: (empty string)
  is_nullable: 1

=head2 list_price

  data_type: 'integer'
  is_nullable: 1

=head2 sale_price

  data_type: 'integer'
  is_nullable: 1

=head2 sale_from_date

  data_type: (empty string)
  is_nullable: 1

=head2 sale_end_date

  data_type: (empty string)
  is_nullable: 1

=head2 item_package_quantity

  data_type: (empty string)
  is_nullable: 1

=head2 offering_can_be_gift_messaged

  data_type: (empty string)
  is_nullable: 1

=head2 offering_can_be_giftwrapped

  data_type: (empty string)
  is_nullable: 1

=head2 missing_keyset_reason

  data_type: (empty string)
  is_nullable: 1

=head2 website_shipping_weight_unit_of_measure

  data_type: (empty string)
  is_nullable: 1

=head2 website_shipping_weight

  data_type: (empty string)
  is_nullable: 1

=head2 item_weight_unit_of_measure

  data_type: (empty string)
  is_nullable: 1

=head2 item_weight

  data_type: (empty string)
  is_nullable: 1

=head2 item_length_unit_of_measure

  data_type: (empty string)
  is_nullable: 1

=head2 item_length

  data_type: (empty string)
  is_nullable: 1

=head2 item_width

  data_type: (empty string)
  is_nullable: 1

=head2 item_height

  data_type: (empty string)
  is_nullable: 1

=head2 item_display_length

  data_type: (empty string)
  is_nullable: 1

=head2 item_display_length_unit_of_measure

  data_type: (empty string)
  is_nullable: 1

=head2 item_display_weight

  data_type: (empty string)
  is_nullable: 1

=head2 item_display_weight_unit_of_measure

  data_type: (empty string)
  is_nullable: 1

=head2 item_display_volume

  data_type: (empty string)
  is_nullable: 1

=head2 item_display_volume_unit_of_measure

  data_type: (empty string)
  is_nullable: 1

=head2 catalog_number

  data_type: (empty string)
  is_nullable: 1

=head2 bullet_point1

  data_type: (empty string)
  is_nullable: 1

=head2 bullet_point2

  data_type: (empty string)
  is_nullable: 1

=head2 bullet_point3

  data_type: (empty string)
  is_nullable: 1

=head2 bullet_point4

  data_type: (empty string)
  is_nullable: 1

=head2 bullet_point5

  data_type: (empty string)
  is_nullable: 1

=head2 specific_uses_keywords1

  data_type: (empty string)
  is_nullable: 1

=head2 specific_uses_keywords2

  data_type: (empty string)
  is_nullable: 1

=head2 specific_uses_keywords3

  data_type: (empty string)
  is_nullable: 1

=head2 specific_uses_keywords4

  data_type: (empty string)
  is_nullable: 1

=head2 specific_uses_keywords5

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_attribute_keywords1

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_attribute_keywords2

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_attribute_keywords3

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_attribute_keywords4

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_subject_keywords1

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_subject_keywords2

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_subject_keywords3

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_subject_keywords4

  data_type: (empty string)
  is_nullable: 1

=head2 thesaurus_subject_keywords5

  data_type: (empty string)
  is_nullable: 1

=head2 target_audience_keywords1

  data_type: (empty string)
  is_nullable: 1

=head2 target_audience_keywords2

  data_type: (empty string)
  is_nullable: 1

=head2 target_audience_keywords3

  data_type: (empty string)
  is_nullable: 1

=head2 generic_keywords1

  data_type: (empty string)
  is_nullable: 1

=head2 generic_keywords2

  data_type: (empty string)
  is_nullable: 1

=head2 generic_keywords3

  data_type: (empty string)
  is_nullable: 1

=head2 generic_keywords4

  data_type: (empty string)
  is_nullable: 1

=head2 generic_keywords5

  data_type: (empty string)
  is_nullable: 1

=head2 platinum_keywords1

  data_type: (empty string)
  is_nullable: 1

=head2 platinum_keywords2

  data_type: (empty string)
  is_nullable: 1

=head2 platinum_keywords3

  data_type: (empty string)
  is_nullable: 1

=head2 platinum_keywords4

  data_type: (empty string)
  is_nullable: 1

=head2 platinum_keywords5

  data_type: (empty string)
  is_nullable: 1

=head2 main_image_url

  data_type: (empty string)
  is_nullable: 1

=head2 swatch_image_url

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url1

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url2

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url3

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url4

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url5

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url6

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url7

  data_type: (empty string)
  is_nullable: 1

=head2 other_image_url8

  data_type: (empty string)
  is_nullable: 1

=head2 fulfillment_center_id

  data_type: (empty string)
  is_nullable: 1

=head2 parent_child

  data_type: (empty string)
  is_nullable: 1

=head2 parent_sku

  data_type: (empty string)
  is_nullable: 1

=head2 relationship_type

  data_type: (empty string)
  is_nullable: 1

=head2 variation_theme

  data_type: (empty string)
  is_nullable: 1

=head2 legal_disclaimer_description

  data_type: (empty string)
  is_nullable: 1

=head2 prop_65

  data_type: (empty string)
  is_nullable: 1

=head2 cpsia_cautionary_statement1

  data_type: (empty string)
  is_nullable: 1

=head2 cpsia_cautionary_statement2

  data_type: (empty string)
  is_nullable: 1

=head2 cpsia_cautionary_statement3

  data_type: (empty string)
  is_nullable: 1

=head2 cpsia_cautionary_statement4

  data_type: (empty string)
  is_nullable: 1

=head2 cpsia_cautionary_description

  data_type: (empty string)
  is_nullable: 1

=head2 safety_warning

  data_type: (empty string)
  is_nullable: 1

=head2 unit_count

  data_type: (empty string)
  is_nullable: 1

=head2 size_name

  data_type: (empty string)
  is_nullable: 1

=head2 color_name

  data_type: (empty string)
  is_nullable: 1

=head2 scent_name

  data_type: (empty string)
  is_nullable: 1

=head2 flavor_name

  data_type: (empty string)
  is_nullable: 1

=head2 skin_type1

  data_type: (empty string)
  is_nullable: 1

=head2 skin_type2

  data_type: (empty string)
  is_nullable: 1

=head2 skin_type3

  data_type: (empty string)
  is_nullable: 1

=head2 skin_type4

  data_type: (empty string)
  is_nullable: 1

=head2 skin_tone1

  data_type: (empty string)
  is_nullable: 1

=head2 skin_tone2

  data_type: (empty string)
  is_nullable: 1

=head2 skin_tone3

  data_type: (empty string)
  is_nullable: 1

=head2 skin_tone4

  data_type: (empty string)
  is_nullable: 1

=head2 skin_tone5

  data_type: (empty string)
  is_nullable: 1

=head2 coverage

  data_type: (empty string)
  is_nullable: 1

=head2 finish_type1

  data_type: (empty string)
  is_nullable: 1

=head2 finish_type2

  data_type: (empty string)
  is_nullable: 1

=head2 material_type1

  data_type: (empty string)
  is_nullable: 1

=head2 material_type2

  data_type: (empty string)
  is_nullable: 1

=head2 material_type3

  data_type: (empty string)
  is_nullable: 1

=head2 hair_type1

  data_type: (empty string)
  is_nullable: 1

=head2 hair_type2

  data_type: (empty string)
  is_nullable: 1

=head2 hair_type3

  data_type: (empty string)
  is_nullable: 1

=head2 target_gender

  data_type: (empty string)
  is_nullable: 1

=head2 item_form

  data_type: (empty string)
  is_nullable: 1

=head2 ingredients

  data_type: (empty string)
  is_nullable: 1

=head2 indications

  data_type: (empty string)
  is_nullable: 1

=head2 directions

  data_type: (empty string)
  is_nullable: 1

=head2 specialty1

  data_type: (empty string)
  is_nullable: 1

=head2 specialty2

  data_type: (empty string)
  is_nullable: 1

=head2 specialty3

  data_type: (empty string)
  is_nullable: 1

=head2 specialty4

  data_type: (empty string)
  is_nullable: 1

=head2 specialty5

  data_type: (empty string)
  is_nullable: 1

=head2 is_adult_product

  data_type: (empty string)
  is_nullable: 1

=head2 are_batteries_included

  data_type: (empty string)
  is_nullable: 1

=head2 batteries_required

  data_type: (empty string)
  is_nullable: 1

=head2 battery_type1

  data_type: (empty string)
  is_nullable: 1

=head2 battery_type2

  data_type: (empty string)
  is_nullable: 1

=head2 battery_type3

  data_type: (empty string)
  is_nullable: 1

=head2 number_of_batteries1

  data_type: (empty string)
  is_nullable: 1

=head2 number_of_batteries2

  data_type: (empty string)
  is_nullable: 1

=head2 number_of_batteries3

  data_type: (empty string)
  is_nullable: 1

=head2 lithium_battery_energy_content

  data_type: (empty string)
  is_nullable: 1

=head2 lithium_battery_packaging

  data_type: (empty string)
  is_nullable: 1

=head2 lithium_battery_voltage

  data_type: (empty string)
  is_nullable: 1

=head2 lithium_battery_weight

  data_type: (empty string)
  is_nullable: 1

=head2 number_of_lithium_ion_cells

  data_type: (empty string)
  is_nullable: 1

=head2 number_of_lithium_metal_cells

  data_type: (empty string)
  is_nullable: 1

=head2 style_name

  data_type: (empty string)
  is_nullable: 1

=head2 package_size_name

  data_type: (empty string)
  is_nullable: 1

=head2 each_unit_count

  data_type: (empty string)
  is_nullable: 1

=head2 total_eaches

  data_type: (empty string)
  is_nullable: 1

=head2 unit_count_type

  data_type: (empty string)
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "item_sku",
  { data_type => "", is_nullable => 1 },
  "external_product_id",
  { data_type => "integer", is_nullable => 1 },
  "external_product_id_type",
  { data_type => "text", is_nullable => 1 },
  "item_name",
  { data_type => "", is_nullable => 1 },
  "brand_name",
  { data_type => "", is_nullable => 1 },
  "manufacturer",
  { data_type => "", is_nullable => 1 },
  "part_number",
  { data_type => "text", is_nullable => 1 },
  "product_description",
  { data_type => "", is_nullable => 1 },
  "feed_product_type",
  { data_type => "", is_nullable => 1 },
  "item_type",
  { data_type => "", is_nullable => 1 },
  "update_delete",
  { data_type => "", is_nullable => 1 },
  "quantity",
  { data_type => "integer", is_nullable => 1 },
  "standard_price",
  { data_type => "integer", is_nullable => 1 },
  "currency",
  { data_type => "", is_nullable => 1 },
  "max_order_quantity",
  { data_type => "", is_nullable => 1 },
  "fulfillment_latency",
  { data_type => "", is_nullable => 1 },
  "''",
  { accessor => "_", data_type => "", is_nullable => 1 },
  "'':1",
  { accessor => "_1", data_type => "", is_nullable => 1 },
  "p_quantity",
  { data_type => "", is_nullable => 1 },
  "product_tax_code",
  { data_type => "", is_nullable => 1 },
  "product_site_launch_date",
  { data_type => "", is_nullable => 1 },
  "restock_date",
  { data_type => "", is_nullable => 1 },
  "is_discontinued_by_manufacturer",
  { data_type => "", is_nullable => 1 },
  "max_aggregate_ship_quantity",
  { data_type => "", is_nullable => 1 },
  "product_tax_code:1",
  { accessor => "product_tax_code_1", data_type => "", is_nullable => 1 },
  "product_site_launch_date:1",
  {
    accessor    => "product_site_launch_date_1",
    data_type   => "",
    is_nullable => 1,
  },
  "merchant_release_date",
  { data_type => "", is_nullable => 1 },
  "list_price",
  { data_type => "integer", is_nullable => 1 },
  "sale_price",
  { data_type => "integer", is_nullable => 1 },
  "sale_from_date",
  { data_type => "", is_nullable => 1 },
  "sale_end_date",
  { data_type => "", is_nullable => 1 },
  "item_package_quantity",
  { data_type => "", is_nullable => 1 },
  "offering_can_be_gift_messaged",
  { data_type => "", is_nullable => 1 },
  "offering_can_be_giftwrapped",
  { data_type => "", is_nullable => 1 },
  "missing_keyset_reason",
  { data_type => "", is_nullable => 1 },
  "website_shipping_weight_unit_of_measure",
  { data_type => "", is_nullable => 1 },
  "website_shipping_weight",
  { data_type => "", is_nullable => 1 },
  "item_weight_unit_of_measure",
  { data_type => "", is_nullable => 1 },
  "item_weight",
  { data_type => "", is_nullable => 1 },
  "item_length_unit_of_measure",
  { data_type => "", is_nullable => 1 },
  "item_length",
  { data_type => "", is_nullable => 1 },
  "item_width",
  { data_type => "", is_nullable => 1 },
  "item_height",
  { data_type => "", is_nullable => 1 },
  "item_display_length",
  { data_type => "", is_nullable => 1 },
  "item_display_length_unit_of_measure",
  { data_type => "", is_nullable => 1 },
  "item_display_weight",
  { data_type => "", is_nullable => 1 },
  "item_display_weight_unit_of_measure",
  { data_type => "", is_nullable => 1 },
  "item_display_volume",
  { data_type => "", is_nullable => 1 },
  "item_display_volume_unit_of_measure",
  { data_type => "", is_nullable => 1 },
  "catalog_number",
  { data_type => "", is_nullable => 1 },
  "bullet_point1",
  { data_type => "", is_nullable => 1 },
  "bullet_point2",
  { data_type => "", is_nullable => 1 },
  "bullet_point3",
  { data_type => "", is_nullable => 1 },
  "bullet_point4",
  { data_type => "", is_nullable => 1 },
  "bullet_point5",
  { data_type => "", is_nullable => 1 },
  "specific_uses_keywords1",
  { data_type => "", is_nullable => 1 },
  "specific_uses_keywords2",
  { data_type => "", is_nullable => 1 },
  "specific_uses_keywords3",
  { data_type => "", is_nullable => 1 },
  "specific_uses_keywords4",
  { data_type => "", is_nullable => 1 },
  "specific_uses_keywords5",
  { data_type => "", is_nullable => 1 },
  "thesaurus_attribute_keywords1",
  { data_type => "", is_nullable => 1 },
  "thesaurus_attribute_keywords2",
  { data_type => "", is_nullable => 1 },
  "thesaurus_attribute_keywords3",
  { data_type => "", is_nullable => 1 },
  "thesaurus_attribute_keywords4",
  { data_type => "", is_nullable => 1 },
  "thesaurus_subject_keywords1",
  { data_type => "", is_nullable => 1 },
  "thesaurus_subject_keywords2",
  { data_type => "", is_nullable => 1 },
  "thesaurus_subject_keywords3",
  { data_type => "", is_nullable => 1 },
  "thesaurus_subject_keywords4",
  { data_type => "", is_nullable => 1 },
  "thesaurus_subject_keywords5",
  { data_type => "", is_nullable => 1 },
  "target_audience_keywords1",
  { data_type => "", is_nullable => 1 },
  "target_audience_keywords2",
  { data_type => "", is_nullable => 1 },
  "target_audience_keywords3",
  { data_type => "", is_nullable => 1 },
  "generic_keywords1",
  { data_type => "", is_nullable => 1 },
  "generic_keywords2",
  { data_type => "", is_nullable => 1 },
  "generic_keywords3",
  { data_type => "", is_nullable => 1 },
  "generic_keywords4",
  { data_type => "", is_nullable => 1 },
  "generic_keywords5",
  { data_type => "", is_nullable => 1 },
  "platinum_keywords1",
  { data_type => "", is_nullable => 1 },
  "platinum_keywords2",
  { data_type => "", is_nullable => 1 },
  "platinum_keywords3",
  { data_type => "", is_nullable => 1 },
  "platinum_keywords4",
  { data_type => "", is_nullable => 1 },
  "platinum_keywords5",
  { data_type => "", is_nullable => 1 },
  "main_image_url",
  { data_type => "", is_nullable => 1 },
  "swatch_image_url",
  { data_type => "", is_nullable => 1 },
  "other_image_url1",
  { data_type => "", is_nullable => 1 },
  "other_image_url2",
  { data_type => "", is_nullable => 1 },
  "other_image_url3",
  { data_type => "", is_nullable => 1 },
  "other_image_url4",
  { data_type => "", is_nullable => 1 },
  "other_image_url5",
  { data_type => "", is_nullable => 1 },
  "other_image_url6",
  { data_type => "", is_nullable => 1 },
  "other_image_url7",
  { data_type => "", is_nullable => 1 },
  "other_image_url8",
  { data_type => "", is_nullable => 1 },
  "fulfillment_center_id",
  { data_type => "", is_nullable => 1 },
  "parent_child",
  { data_type => "", is_nullable => 1 },
  "parent_sku",
  { data_type => "", is_nullable => 1 },
  "relationship_type",
  { data_type => "", is_nullable => 1 },
  "variation_theme",
  { data_type => "", is_nullable => 1 },
  "legal_disclaimer_description",
  { data_type => "", is_nullable => 1 },
  "prop_65",
  { data_type => "", is_nullable => 1 },
  "cpsia_cautionary_statement1",
  { data_type => "", is_nullable => 1 },
  "cpsia_cautionary_statement2",
  { data_type => "", is_nullable => 1 },
  "cpsia_cautionary_statement3",
  { data_type => "", is_nullable => 1 },
  "cpsia_cautionary_statement4",
  { data_type => "", is_nullable => 1 },
  "cpsia_cautionary_description",
  { data_type => "", is_nullable => 1 },
  "safety_warning",
  { data_type => "", is_nullable => 1 },
  "unit_count",
  { data_type => "", is_nullable => 1 },
  "size_name",
  { data_type => "", is_nullable => 1 },
  "color_name",
  { data_type => "", is_nullable => 1 },
  "scent_name",
  { data_type => "", is_nullable => 1 },
  "flavor_name",
  { data_type => "", is_nullable => 1 },
  "skin_type1",
  { data_type => "", is_nullable => 1 },
  "skin_type2",
  { data_type => "", is_nullable => 1 },
  "skin_type3",
  { data_type => "", is_nullable => 1 },
  "skin_type4",
  { data_type => "", is_nullable => 1 },
  "skin_tone1",
  { data_type => "", is_nullable => 1 },
  "skin_tone2",
  { data_type => "", is_nullable => 1 },
  "skin_tone3",
  { data_type => "", is_nullable => 1 },
  "skin_tone4",
  { data_type => "", is_nullable => 1 },
  "skin_tone5",
  { data_type => "", is_nullable => 1 },
  "coverage",
  { data_type => "", is_nullable => 1 },
  "finish_type1",
  { data_type => "", is_nullable => 1 },
  "finish_type2",
  { data_type => "", is_nullable => 1 },
  "material_type1",
  { data_type => "", is_nullable => 1 },
  "material_type2",
  { data_type => "", is_nullable => 1 },
  "material_type3",
  { data_type => "", is_nullable => 1 },
  "hair_type1",
  { data_type => "", is_nullable => 1 },
  "hair_type2",
  { data_type => "", is_nullable => 1 },
  "hair_type3",
  { data_type => "", is_nullable => 1 },
  "target_gender",
  { data_type => "", is_nullable => 1 },
  "item_form",
  { data_type => "", is_nullable => 1 },
  "ingredients",
  { data_type => "", is_nullable => 1 },
  "indications",
  { data_type => "", is_nullable => 1 },
  "directions",
  { data_type => "", is_nullable => 1 },
  "specialty1",
  { data_type => "", is_nullable => 1 },
  "specialty2",
  { data_type => "", is_nullable => 1 },
  "specialty3",
  { data_type => "", is_nullable => 1 },
  "specialty4",
  { data_type => "", is_nullable => 1 },
  "specialty5",
  { data_type => "", is_nullable => 1 },
  "is_adult_product",
  { data_type => "", is_nullable => 1 },
  "are_batteries_included",
  { data_type => "", is_nullable => 1 },
  "batteries_required",
  { data_type => "", is_nullable => 1 },
  "battery_type1",
  { data_type => "", is_nullable => 1 },
  "battery_type2",
  { data_type => "", is_nullable => 1 },
  "battery_type3",
  { data_type => "", is_nullable => 1 },
  "number_of_batteries1",
  { data_type => "", is_nullable => 1 },
  "number_of_batteries2",
  { data_type => "", is_nullable => 1 },
  "number_of_batteries3",
  { data_type => "", is_nullable => 1 },
  "lithium_battery_energy_content",
  { data_type => "", is_nullable => 1 },
  "lithium_battery_packaging",
  { data_type => "", is_nullable => 1 },
  "lithium_battery_voltage",
  { data_type => "", is_nullable => 1 },
  "lithium_battery_weight",
  { data_type => "", is_nullable => 1 },
  "number_of_lithium_ion_cells",
  { data_type => "", is_nullable => 1 },
  "number_of_lithium_metal_cells",
  { data_type => "", is_nullable => 1 },
  "style_name",
  { data_type => "", is_nullable => 1 },
  "package_size_name",
  { data_type => "", is_nullable => 1 },
  "each_unit_count",
  { data_type => "", is_nullable => 1 },
  "total_eaches",
  { data_type => "", is_nullable => 1 },
  "unit_count_type",
  { data_type => "", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-05-27 11:12:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:g+Bq7BPvLCohT/tmyuteCA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
