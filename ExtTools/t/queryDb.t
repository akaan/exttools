use warnings;
use lib "./lib";
use BmcApp;
use Data::Dumper;

my $app      = BmcApp->new();
my $name     = 'BMC::BmcStyle';
my $appmodel = $app->model($name);
my $rs       = $appmodel->search(
	undef,
	{
		prefetch => {
			'bmc_style_color_matrixes' => 'style_code',
		}
	}
);
my $r =undef;
while ($r = $rs->next) {
	my @rrs = $r->bmc_style_color_matrixes->all();
	foreach (@rrs) {
		printf " rs = %s, [%s]\n", $_->code, $_->color_code->name;
	}
}

1;
